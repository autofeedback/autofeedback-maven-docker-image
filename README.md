# AutoFeedback Maven Docker image

Docker image that runs a Maven build for AutoFeedback, using the new APIs available since 0.6.0.

To try out this image, first bring up the [AutoFeedback web app](https://gitlab.com/autofeedback/autofeedback-webapp) and generate a pair of single-use tokens for a job:

```bash
pushd autofeedback-webapp
./dev-compose.sh up -d
# ... set up an assessment and make a submission: note the job ID ...

./dev-artisan.sh autofeedback:issue-job-tokens JOB_ID

# ... note down the two single-use tokens, specific to this job ID ...
```

After this, run a Maven build for autofeedback by using the script in this repository:

```bash
popd
pushd autofeedback-maven-docker-image
./run-local-build.sh HOST:PORT JOB_ID DOWNLOAD_TOKEN RESULTS_TOKEN
```

* `HOST:PORT` should point to the AutoFeedback webapp, using an IP address that is valid from a Docker container (e.g. the gateway IP address of the host machine in the AutoFeedback network).
* `JOB_ID` is that same job ID we used before.
* `DOWNLOAD_TOKEN` is the single-use token for downloading the job build ZIP (usually of the form 'ID|longString').
* `RESULTS_TOKEN` is a similar value with the single-use token for uploading the build results. AutoFeedback will compute marks based on these build results.
