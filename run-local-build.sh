#!/bin/bash

set -e

if test "$#" != 4; then
  echo "Usage: $0 host:port job_id token_build token_store"
  exit 1
fi

HOST="$1"
JOB_ID="$2"
AF_BUILD_ZIP_TOKEN="$3"
AF_STORE_RESULTS_TOKEN="$4"

URL_PREFIX="http://${HOST}/api/jobs/${JOB_ID}"

./compose.sh build && ./compose.sh run \
  -e "AF_BUILD_ZIP_URL=$URL_PREFIX/buildZip" \
  -e "AF_BUILD_ZIP_TOKEN=$AF_BUILD_ZIP_TOKEN" \
  -e "AF_STORE_RESULTS_URL=$URL_PREFIX/results" \
  -e "AF_STORE_RESULTS_TOKEN=$AF_STORE_RESULTS_TOKEN" \
  maven
