#!/bin/bash


### ZIP DOWNLOAD


if [ -z "${AF_BUILD_ZIP_URL+x}" ]; then
	echo "AF_BUILD_ZIP_URL is unset: cannot download build ZIP" >&2
	exit 1
fi

BUILD_ZIP_HEADERS=af-buildzip.headers
if [ -z "${AF_BUILD_ZIP_TOKEN+x}" ]; then
	echo "AF_BUILD_ZIP_TOKEN is unset: cannot download build ZIP" >&2
	exit 3
else
	cat >$BUILD_ZIP_HEADERS <<EOF
Accept: application/json
Authorization: Bearer ${AF_BUILD_ZIP_TOKEN}
EOF
fi
cat "$BUILD_ZIP_HEADERS"

# Download and unzip the build ZIP
BUILD_ZIP=build.zip
curl -H "@${BUILD_ZIP_HEADERS}" "${AF_BUILD_ZIP_URL}" --output build.zip
unzip -n build.zip
rm -f build.zip

# Unset the variables that are no longer needed
unset AF_BUILD_ZIP_URL
unset AF_BUILD_ZIP_TOKEN
rm "$BUILD_ZIP_HEADERS"


### BUILD


RESULTS_DIRECTORY=af_results
mkdir "$RESULTS_DIRECTORY"

# Run the build from a clean Bash login shell (without remaining storeResults token)
env -i \
  HOME="$HOME" \
  MAVEN_HOME="$MAVEN_HOME" \
  MAVEN_CONFIG="$MAVEN_CONFIG" \
  JAVA_HOME="$JAVA_HOME" \
  AUTOFEEDBACK=1 \
  bash -l -c 'mvn -B test' \
  1> >(tee "$RESULTS_DIRECTORY/stdout.txt") \
  2> >(tee "$RESULTS_DIRECTORY/stderr.txt")

MAVEN_RESULT=$?


### ZIP RESULTS


SUREFIRE_DIR=target/surefire-reports
if test -d "$SUREFIRE_DIR"; then
	mkdir "$RESULTS_DIRECTORY/junit"
	find target/surefire-reports -name 'TEST-*.xml' -exec mv '{}' "$RESULTS_DIRECTORY/junit" \;
fi


move_source() {
	SOURCE=$1
	DIR=$2

	if test -d "$DIR"; then
		mv "$DIR" "$RESULTS_DIRECTORY/$SOURCE"
	fi
}

move_source jacoco target/site/jacoco
move_source jacoco-it target/site/jacoco-it
move_source pit target/site/pit-reports

(cd $RESULTS_DIRECTORY && zip -r ../results.zip .)


### UPLOAD RESULTS


if [ -z "${AF_STORE_RESULTS_URL+x}" ]; then
	echo "AF_STORE_RESULTS_URL is unset: cannot store results" >&2
	exit 2
fi

STORE_RESULTS_HEADERS=af-storeresults.headers
if [ -z "${AF_STORE_RESULTS_TOKEN+x}" ]; then
	echo "AF_STORE_RESULTS_TOKEN is unset: cannot store results" >&2
	exit 4
else
	cat >"$STORE_RESULTS_HEADERS" <<EOF
Accept: application/json
Authorization: Bearer $AF_STORE_RESULTS_TOKEN
EOF
fi

curl -H "@${STORE_RESULTS_HEADERS}" \
  -F "resultsfile=@results.zip" \
  -F "statuscode=$MAVEN_RESULT" \
  ${AF_STORE_RESULTS_URL}
